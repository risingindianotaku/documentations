---
title: Misc stuff
description: 
published: 1
date: 2020-04-14T19:57:34.458Z
tags: 
---

Other (misc) stuff

## Available commands
- `/runs`: Showes random runs message
- `/cancel`: Disables current state. Can help in cases if Sophie not responing on your message.
{.grid-list}


## Other commands pages
- [Privileged users commands (operators, owner)](privileged)
{.links-list}