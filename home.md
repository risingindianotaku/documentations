---
title: Home
description: 
published: 1
date: 2020-05-12T05:41:48.396Z
tags: 
---

# Sophie Telegram Bot
> Sophie is a modern and fast Telegram chat manager.
{.is-info}

---

**Sophie's features over other bots:**
- Modern
- Fast
- Modular
- Fully asynchronous
- Fully open-source
- Frequently updated
- Sophie respects your data privacy, we constantly improving privacy and security in Sophie

Add Sophie to your chat:
- [Sophie Bot](https://t.me/rSophieBot)
- [Sophie Support chat](https://t.me/SophieSupport)
{.links-list}


