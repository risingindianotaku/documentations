---
title: Federations
description: 
published: 1
date: 2020-05-26T03:37:21.022Z
tags: 
---

## Avaible commands:
>**This help you to read the wiki.**
**()** - required argument
**(user)** - required but you can reply on any user's message instead
**(file)** - required file, if file isn't provided you will be entered in file state, this means Sophie will wait file message from you. Type /cancel to leave from it.
**(? )** - additional argument
{.is-info}

#### Only Federation owner:
- `/fnew (name)` or `/newfed (name)`
{.grid-list}

Use this command to create the federation named `name` whatever you give to me !
> Example: `/fnew myfed`
Sophie will reply bunch of info like creator, name etc.. and `fed ID` , this ID will be used to join fed

> To make our databases clean, Sophie from now only allow ONE federation per account
{.is-warning}

---

- `/frename (?Fed ID) (new name)`
{.grid-list}

It would be boring if federations can't be renamed so Sophie now supports renaming of federations 
> Example: `/frename new name`

> New name should be different from existing one.
{.is-warning}

---

- `/fdel (?Fed ID)` or `/delfed (?Fed ID)`
{.grid-list}

Yeah! Sophie now only allow ONE Federation for an user, that wouldn't possible without this option
> Once done this action CANNOT be undone
{.is-danger}

> Example: `/fdel (?Fed id)` or just `/fdel` if chat is in the federation you want to delete.
Sophie will reply with inline button asking again for your confirmation !

---

- `/fjoin (Fed ID)` or `/joinfed (Fed ID)`
{.grid-list}

As the command, This is used to join federation
> Example: `/fjoin 60b4b742-c6d7-4ef8-b207-0f2f6800e62e`
thats wierd alphanumeric is the `fed ID`

---

- `/fleave` or `/leavefed`
{.grid-list}

Well if you join something, you may have to leave it! Use this command to leave the federation your chat joined
> Example: Just `/leavefed`

---

- `/fpromote (user) (?Fed ID)`
{.grid-list}

A federation cannot be controlled just by creator for smooth run, right? add administrators to your federation using this command
> Example: `/fpromote @username` 
Or `/fpromote @username 60b4b742-c6d7-4ef8-b207-0f2f6800e62e`.
Argument `Fed ID` doesn't need if you are in the chat that is following your federation.

---

- `/fdemote (user) (?Fed ID)`
{.grid-list}

Demotes the Fed admin 
> Example: `/fdemote @username` 
Or `/fdemote @username 60b4b742-c6d7-4ef8-b207-0f2f6800e62e`.
Argument `Fed ID` doesn't need if you are in the chat that is following your federation.

---

- `/fsub (Fed ID)`
{.grid-list}

It is not possible that only one federation can ban all spammers, So one federation can subscribe to another federation using this command.This would ban the users banned in subscribed fed

> Example: `/fsub 41ff1283-24d2-458e-8189-40d0a39aad26`
Now my fed is subscribed to this `fed`

---

- `/funsub (Fed ID)`
{.grid-list}

Not satisfied with performance of subscribed fed? Unsubscribe them right away using this command

> Example: `/funsub 41ff1283-24d2-458e-8189-40d0a39aad26`

---

- `/fsetlog (?Fed ID)` or `/setfedlog (?Fed ID)`
{.grid-list}

Uhm need to log all Fed actions such as ban, chats joined etc..? Use this command to do it.
> Example: Go to the chat you want to use it as logging group
and `/fsetlog 60b4b742-c6d7-4ef8-b207-0f2f6800e62e`
or just `/fsetlog` if the chat is in your fed !

---

- `/funsetlog (?Fed ID)` or `/unsetfedlog (?Fed ID)`
{.grid-list}

Bored of reading logs? Want to change logging group? Use this command to stop logging of fed actions.

> Example: `/funsetlog 60b4b742-c6d7-4ef8-b207-0f2f6800e62e` or just `/funsetlog` if the chat already is in your fed

---

- `/fexport (?Fed ID)`
{.grid-list}

If you want to give fbanned users list to another fed or just for backup. Use this command
> Example: `/fexport 60b4b742-c6d7-4ef8-b207-0f2f6800e62e`
Or just `/fexport` if the chat is in your fed
Sophie will reply you with JSON file.

>  If you are using this file to import fbans to your fed, We recommend you not to use instead please use `fed subscription` method !  fbans in fbanlist may get unfbanned in future thus user may get fbanned in your fed for no reason as imported fbans won't get updated in real-time and fbanlist doesn't contain unfbanned data.
{.is-info}

---

- `/fimport (?Fed ID) (file)`
{.grid-list}

Use this command to import the above mentioned JSON file, 
> Example:
There are 2 methods for this, You can upload JSON file and reply to that `/fimport 41ff1283-24d2-458e-8189-40d0a39aad26` or just `fimport`
The second method is -
User: `/fimport 41ff1283-...` or just `/fimport`
Sophie will now say you to upload the file. Now upload the file, Voila! You made it.
{.grid-list}
#### Avaible for Fed admins:
- `/fchatlist (?Fed ID)` or `/fchats (?Fed ID)`
{.grid-list}

Will send the list of chats in fed
> Example: `/fchats 41ff1283-24d2-458e-8189-40d0a39aad26`
Or just `/fchats`

---

- `/fadminlist (?Fed ID)` or `/fadmins (?Fed ID)`
{.grid-list}

Will send the list fed admins.
> Example: `/fadminlist 41ff1283-24d2-458e-8189-40d0a39aad26`
Or just `/fadminlist`

---

- `/fban (user) (?Fed ID) (?reason)`: Bans user in the Fed and Feds which subscribed on this Fed
{.grid-list} 

Use this command to ban users who spams throughout the fed, User will be banned in all chats of the fed and in all fed which subscribed to this fed. `user` argument is optional, you can reply to the offended user's message. Also `fed ID` is not neccessary if chat is in the fed that you willing to ban the offended user

> Example: `/fban @username reason here`

---
- `/sfban (user) (?Fed ID) (?reason)`
{.grid-list}

This will silently fban user! ban notification, replied message will be deleted automatically after 5 seconds. This help you to keep your chat clean

---
- `/unfban (user) (?Fed ID) (?reason)` 
{.grid-list}

Just like above ! User will unbanned in all chats of the fed and fed which subscribed on this fed

#### Avaible for all users:
- `/finfo (?Fed ID)`
{.grid-list}

Will send you the information and statistics about the Federation