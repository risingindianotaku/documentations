---
title: Running by manual
description: 
published: 1
date: 2020-04-09T11:35:10.614Z
tags: 
---

## Manual way 
### Requirements 
+ GNU/Linux based OS
+ git
+ Python 3.8+ (with pip installed)
+ MongoDB (or MongoDB Atlas account)
+ Redis

### Cloning this repo
``` bash
	git clone https://github.com/MrYacha/SophieBot  
```

### Installing requirements  
``` bash
	cd SophieBot
	pip install -r requirements.txt
```
  
### Setting config  
  
+ Go to SophieBot/data  
+ Rename bot_conf.yaml.example to bot_conf.yaml  
+ Open in text editor  
+ Set configs as needed  
  
### Running  
``` bash
	cd SophieBot
	python -m sophie_bot
```