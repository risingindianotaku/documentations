---
title: Running Sophie
description: 
published: 1
date: 2020-05-20T08:48:39.304Z
tags: 
---

Sophie can be deployed in 2 ways

- [Docker way](docker)
- [Manual way](manually)
{.links-list}

Docker way is highly recommended, containers have many benefits, like: easy to setup, fully isolation between system and container