---
title: Running Sophie by Docker
description: 
published: 1
date: 2020-04-20T13:04:58.065Z
tags: 
---

## Running by Docker

> In this instruction used Podman over Docker, using podman is recommended, but in case if you want to use Docker, just follow this guide and replace `podman` to `sudo docker`
{.is-info}

### Requirements
+ Installed Docker / Podman
+ Installed wget


### Cloning prebuilded container
``` bash
	podman pull registry.gitlab.com/mryacha/sophie:unstable
```

### Creating a pod 
``` bash
	podman pod create --name sophie
```
### Running Redis and MongoDB
``` bash
	podman run --detach --name redis-server --pod sophie redis:alpine
	podman run --detach --name mongo-server --pod sophie mongo:latest
```

### Creating a directory
``` bash
	mkdir ~/Sophie
  cd ~/Sophie
```

### Downloading a config
``` bash
	wget https://gitlab.com/MrYacha/sophie/-/raw/unstable/data/bot_conf.yaml.example bot_conf.yaml
```

### Setting a config
+ Open bot_conf.yaml
+ Set mongo_conn to "localhost"  
+ Set redis_conn to "localhost"  
+ Set other configs as needed

### Run a container
```
  podman run --detach --name sophie -v /home/yacha/Sophie/data/:/opt/sophie_bot/data --pod  sophie registry.gitlab.com/mryacha/sophie:unstable
```

## Updating
+ Pull container
+ Stop and remove container
+ [run container again](/dev/docker#run-a-container)
Example:
``` bash
	podman pull registry.gitlab.com/mryacha/sophie:unstable
  podman stop sophie
  podman rm sophie
  podman run -d --name sophie ...
```

## Building container manually

Automatically with new commits GitLab CI updating official container, so it will be always up-to date, but if you wish to manually build docker container here is instruction.

Requirements 
+ git
+ installed Docker / Podman

``` bash
	git clone https://gitlab.com/MrYacha/sophie --branch unstable SophieBot
	cd SophieBot
	podman build . -t sophie
```