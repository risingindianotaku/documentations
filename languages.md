---
title: Languages
description: 
published: 1
date: 2020-05-13T15:46:16.119Z
tags: 
---

This module is dedicated towards utlising Sophie's localization feature! You can also [contribute](https://crowdin.com/project/sophiebot) for improving localization in Sophie!

## Available commands are:
- `/lang`
{.grid-list}

Use this command to change language of Sophie in your chat

> **Example:**  `/lang`
> Sophie will send you bunch of inline buttons where you can select your prefered language interatively without any hassles!
