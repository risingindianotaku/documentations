---
title: Imports / exports
description: Let's archive all your chat's data in text file
published: 1
date: 2020-05-20T08:43:22.667Z
tags: 
---

Sometimes you want to see all of your data in your chats or you want to copy your data to another chats or you even want to swift bots, in all these cases imports/exports for you!

## Available commands
- `/export`: Export chat's data to JSON file
- `/import`: Import JSON file to chat
{.grid-list}

Notes: Exporting / importing avaible every 2 hours to prevent flooding.

## More use cases
### Batch editing
If you want to edit many notes or filters, you can export, make edits and import again.
### Bakuping data
You can backup chat settings before editing something.
### Copy to other chats
For example if you want to copy notes or rules beetwin chats you can do it.

## Moving from Sophie
Sophie has own exports format which may not be compatible with other bots. Developers can intregrate Sophie's format in their bot without any hassle! [Dev specifications of export file](/dev/export_file).