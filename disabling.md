---
title: Command disabling
description: *This text is disabled*
published: 1
date: 2020-04-09T11:49:16.376Z
tags: 
---

Sometimes you can just bored that users spamming with specify Sophie's command, thats why disabling exits, with diasbling you can easely disable most commands.

## Available commands
- `/disable (command)`: Disables command
- `/enable (command)`: Enables command
- `/disableable`: Show disable-able commands
- `/disabled`: Show disabled commands in this chat
- `/enableall`: Enables all disabled commands in chat
{.grid-list}