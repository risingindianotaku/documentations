---
title: Contributing
description: 
published: 1
date: 2020-04-14T22:05:23.262Z
tags: 
---

Here is few ways to help Sophie

- Help to translate Sophie on your language
Crowdin - https://crowdin.com/project/sophiebot
Ask @MrYacha (Telegram) if there is no your language

- If you know python, you can help by contributing in the code
GitLab link - https://gitlab.com/MrYacha/sophie
Current tasks - https://gitlab.com/MrYacha/sophie/-/boards

- If you don't like to do anything, you can help by donating money
OpenCollective - https://opencollective.com/orangefox