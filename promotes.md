---
title: Promotes / Demotes
description: 
published: 1
date: 2020-05-13T19:31:36.379Z
tags: 
---

## Available commands
- `/promote (user) (?admin's title)`: Promotes the user to admin.
- `/demote (user)`: Demotes the user from admin.
{.grid-list}