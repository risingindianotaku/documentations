---
title: Changelogs
description: Evolution of Sophie
published: 1
date: 2020-05-13T19:39:50.896Z
tags: 
---

## Sophie v2.0
> Released 13th May 2020
{.is-info}


#### General changes
- Now Sophie checks admin rights to make sure the user has  rights to perform the action. For example, if the user doesn't have permission to ban users, he/she can't use `/ban` command
- Sophie's help moved to the https://wiki.sophiebot.gq, help module is removed
- Sudo users were removed and converted to operators
- Global bans were removed
- Improved perfomance a lot
- Fixed English localization
- Fixed issue when the command can be recognized not on the first line of the message
- Added [Imports/exports feature](/imports_exports)
- Added [stickers module](/stickers)
- Added [locks module](/locks)

#### [Notes](/notes)
- Support saving inline URL buttons from other bots
- Support resaving inline callblacks buttons from Sophie's messages
- Encryption method was changed to default MongoDB's one, it makes the decryption process faster and simpler
- Improved support for saving already formatted messages (Telegram X issue fixed)
- Underline and strikethrough now supported
- Implemented cashtags instead of old note settings, for example, instead of `[format:html]` you have to `%PARSEMODE_HTML`, old parsing settings left for backward compatibility
- Implemented new notes variables - {date}, {time}, {timedate}
- Forbidden to save blank notes
- Implemented `/clearall` command.
- Implemented `/search` command to do a text search query in notes
- Implemented sorting by note name in `/notes` command feature
- Implemented notes aliases features
- Implemented way to remove many notes by one command.
- From now buttonnote in chats will redirect in Sophie's PM
- From now clicking on buttonnote in Sophie's PM will edit message instead of sending new
- Minor strings changes
- 'buttonnote' now will redirect in Sophie's PM instead of trying PM you first
- And many other notes changes and bug fixes

#### [Federations](/feds)
- Implemented federation subscriptions
- Implemented federation logs
- Sophie now can fban users which she couldn't get (works only with user ID as argument)
- Implemented `/fadmins` command
- Implemented `/fimport` and `/fexport` command
- Implemented `/freneame` command
- Added timestamp for every federation ban
- Fixed bug when `reason text` sometimes splitted to `reasontext`

#### [Warnings](/warns)
- `/delwarns` now aliased to `/resetwarns`
- Many improvements and fixes

#### [Misc](/misc)
- `/stats` are now allowed only for operators
- `/runs` can be disabled for now

#### [Restrictions](/restriction)
- Added silent commands for bans, mutes and kicking, like `/skick` or `/sban`
- Now after kicking or banning Sophie will delete 'Sophie removed X' service message
- Removed `/kickme` command

#### [Greetings](/greetings)
- Module was fully rewrited
- `/setwelcome` now doesn't require note with welcome message in
- Now to disable welcome you have to use `/setwelcome off`
- Updated welcome security: added security levels (button, math, captcha).
- Added security note with which user will be propmted to validate his status in bot's PM.
- Now if welcome security is enabled, Sophie won't send welcome message in chat, but she will send in user's PM after they proved them as human.
- Added /welcomemute feature


### [Warnings](/warns)
- Implemented /warnaction
- Many improvements and fixes


### [Filters](/filters)
- Now /addfilter gets keyword by argument instead of asking it by state
- Filters now fully modular and support inegration by other modules
- Filters now works in pm if user is connected by /connect feature
- Many improvements and fixes

### Users
- Implemented `/admincache` command, which updates admins cache
- From now `/adminlist` won't clear admins cache

#### [Languages](/en/languages)
- Many improvements and fixes

#### [Connections](/connections)
- Many improvements and fixes
- Added `/allowusersconnect` to disallow users to connect to chat (admins will be able to connect to the chat)
- If user hasn't started dialog with Sophie, but connected to the chat directly, after starting bot Sophie will reply with the 'connected successfully' message

#### Errors reporting and crashlytics
- Sophie is now using sentry.io as crashlytics storage
- Removed the raw crash files feature
- Sophie won't spam with same error message anymore

#### Code changes
- Mainly we focus to support GNU/Linux OS or Docker, but Sophie may work on other OSs too.
- Bot config file changed to YAML
- Removed support of Telethon as commands register
- Sophie is now fully modular, modules ain't depending on each other
- Migrated from MongoDB Pymongo to MongoDB Motor
- Components were removed
- Dockerfile base image was changed to an Alpine instead of Ubuntu
- Translations were converted to YAML files instead of JSON
- Logging changed to a loguru library

And tons of other edits and fixes

### For selfhosting
- From now we are focused on Docker image, so you can easely run it by Docker/podman
- Docker image can be downloaded and runned from GitLab, no need to build Docker image manually anymore.
- GitLab auto CI updates docker images with every commit, so docker image will be always latest
- In v2 introduced database structure migrator, this inbuilt tools will automatically update database for new versions.
- From now logs will be saved in `Logs/` directory 
