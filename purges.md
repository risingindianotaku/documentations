---
title: Purges
description: Nuke garbage from your chat
published: 1
date: 2020-03-27T23:30:12.150Z
tags: 
---

## Available commands are:
- `/purge`: Deletes all messages from the message you replied to, to the current message.
- `/del`: Deletes the message you replied to and your "`/del`" command message.
{.grid-list}
