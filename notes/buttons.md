---
title: Buttons
description: 
published: 1
date: 2020-05-19T15:37:01.785Z
tags: 
---

Here you will know how to setup buttons in your note, welcome note, etc..

# Buttons

There are different types of buttons!
> Due to current Implementation adding invalid button syntax to your note will raise error! This will be fixed in next major version
{.is-danger}


## . Button Note

> Don't confuse this title with notes with buttons 😜
{.is-info}

This types of button will allow you to show specific notes to users when they click on buttons!  

You can save note with button note without any hassle by adding below line to your note ( Don't forget to replace `notename` according to you 😀)

`[Button Name](btnnote:notename)`

> **Example**
> Here we gonna save a note `patches` with **buttonnote** - which will have two buttons for another notes `patchV1` and `patchV2`
> ```
>/save patches
>Patches V1 and V2 
>[PatchV1](btnnote:patchV1)
>[PatchV2](btnnote:patchV2)
>```
> Voila! Its that simple to create buttonnote 🙂

## . URL Button
Ah as you guessed! This method is used to add URL button to your note. With this you can redirect users to your website or even redirecting them to any channel, chat or messages!

You can add URL button by adding following syntax to your note

`[Button Name](btnurl:https://your.link.here)`

> **Example**
> Here we gonna add note `wiki` which will redirect users to Sophie's Wiki
>```
> /save wiki
> Click below button for help!
> [Wiki](btnurl:https://wiki.sophiebot.gq)
>```
> Simple!