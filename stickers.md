---
title: Stickers
description: Well, I can’t attach a good sticker in this short text line, so please imagine that this is a great sticker.
published: 1
date: 2020-04-09T11:48:41.328Z
tags: 
---

Stickers are the best way to show emotion.

## Available commands
- `/getsticker`: Give the sticker image and ID.
{.grid-list}


