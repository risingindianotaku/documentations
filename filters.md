---
title: Filters
description: 
published: 1
date: 2020-05-07T12:27:01.626Z
tags: 
---

Filter module is great for everything ! filter in here is used to filter words or sentences in your chat - send notes, warn, ban those! 
### General (Admins)
- `/addfilter (word/sentence)`
{.grid-list}

As in command this is used to add filter. To avoid hassle when setting up the the filter, Sophie will take you through an interactive setup !

As of now, there is 6 actions that you can do -

- `Send a note`
- `Warn the user`
- `Ban the user`
- `Mute the user`
- `tBan the user`
- `tMute the user`

> A filter can support multiple actions !
{.is-info}

Ah if you don't understand what this actions are for? - actions says bot what to do when the given `word/sentence` is triggered

### Example of setting up filter
 
Step for setting each actions differs - most of them identical !
Here we are setting up filter for word `foo`
![filter_setup.png](/filter_setup.png)
Actions except `send a note` , `tmute the user` , `tban the user` are directly saved. These exceptioned actions like `send a note` require the note that need to be send. Similarly `tmute the user` and `tban the user` requires time

**Saving filter with action `Send a note`**

Here, I will save `foo` filter with action `Send a note` , My note name is `note` and it contain text `bar`, after clicking `Send a note` button - This is how I setup `foo` filter 
![saving_note_filter.png](/saving_note_filter.png)
It was that easy, right? Now look how to set up `tBan the user` or `tMute the user` - Both follow same procedure

**Saving filter with action `tBan the user`**

I will setup `foo` filter with action `tBan the user` or `tMute the user` for `10m` (10 minutes), after clicking button -
![saving_time_filter.png](/saving_time_filter.png)
Again that was so easy too 😉

######
- `/delfilter (filter)`
{.grid-list}

Ah you don't like your filter? Use this command to delete the filter
> **Example**
> 
> Here I gonna delete my `foo` filter - It just need `/delfilter foo` !
>
> If there's more than one action for a word/sentence, Sophie will give you options and you can select which one you want to delete

### Available for all users

- `/filters` or `/listfilters`
{.grid-list}

You want to know all filter of your chat/ chat you joined? Use this command. It will list all filters along with specified actions !