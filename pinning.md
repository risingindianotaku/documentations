---
title: Pinning
description: 
published: 1
date: 2020-03-27T23:29:09.562Z
tags: 
---

## Available commands
- `/pin`: Pins the message
- `/unpin`: Unpins current pinned message
{.grid-list}