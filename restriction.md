---
title: User's restrictions
description: 
published: 1
date: 2020-03-27T23:30:53.423Z
tags: 
---

General admin's rights is restrict users and control their rules with this module you can easely do it.

## Available commands
#### Kicks
- `/kick`: Kicks a user
- `/skick`: Silently kicks
{.grid-list}
#### Mutes
- `/mute`: Mutes a user
- `/smute`: Silently mutes
- `/tmute (time)`: Temprotary mute a user
- `/stmute (time)`: Silently temprotary mute a user
- `/unmute`: Unmutes the user
{.grid-list}
#### Bans
- `/ban`: Bans a user
- `/sban`: Silently bans
- `/tban (time)`: Temprotary ban a user
- `/stban (time)`: Silently temprotary ban a user
- `/unban`: Unbans the user
{.grid-list}

## How silently functions works
TODO
### Use cases
TODO

## Examples of temprorary restricting