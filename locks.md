---
title: Locks
description: 
published: 1
date: 2020-05-20T08:46:19.865Z
tags: 
---

Use this feature to block users from sending specific message types to your group!

## Available commands are:
- `/locks` or `/locktypes` 
{.grid-list}

Use this command to know current state of your locks in your group!
###
- `/lock (locktype)`
{.grid-list}

Use this command to lock certain message types,
there are 4 locktypes -
`all` - will lock all kind of messages
`media` - will lock media messages
`polls` - will lock polls
`others`- all others (gifs, stickers)

> **Example:** `/lock media`
> This will lock media messages such as video, audio etc..
> Do just like this for using other option (replace `media` with your option and voila!)
###
- `/unlock (locktype)`
{.grid-list}

Well as it says it will unlock previously locked messagetype!

> **Example:** `/unlock all`