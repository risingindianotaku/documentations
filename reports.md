---
title: Reporting
description: Kyle, I will tell mom about this
published: 1
date: 2020-04-09T11:54:24.955Z
tags: 
---

With Reports all users can inform admin about the incident

## Available commands
- `/report (?text)`: Reports
- `@admins`: Same as above, but not a clickable
{.grid-list}

TIP: You always can disable reporting by disabling module