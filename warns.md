---
title: Warns
description: 
published: 1
date: 2020-04-15T15:02:50.403Z
tags: 
---

You can keep your members from getting out of control using this feature!

## Available command
#### General (Admins)
- `/warn (?user) (?reason)` 
{.grid-list}

Use this command to warn the user! you can mention or reply to the offended user and add reason if needed
> Example : `/warn @username some reason`

- `/delwarns` or `/resetwarns`
{.grid-list}

This command is used to delete all the warns user got so far in the chat
> Example : `/delwarns @username`
#### Warnlimt (Admins)
- `/warnlimit (new limit)`
{.grid-list}

Not all chats want to give same maximum warns to the user, right? This command will help you to modify default maximum warns. Default is 3
> Example : `/warnlimit 6`

> The new warnlimit should be greater than 1 and less than 10,000
{.is-warning}

#### Warnaction (Admins)
- `/warnaction (mode - ban|mute|tmute) (?time)`
{.grid-list}

Well again, not all chats want to ban (default) users when exceeded maximum warns so this command will able to modify that.
	Current supporting actions are `ban` (default one), `mute`, `tmute`. The tmute mode require `time` argument as you guessed.
> Example : `/warnaction tmute 20m` this will mute user for 20 minutes.
and `/warnaction ban` also works!
#### Available for all users
- `/warns (?user)`
{.grid-list}

Use this command to know number of warns and information about warns you got so far in the chat. To use yourself you doesn't require `user` argument.
> Example : `/warns @username` or just `/warns`